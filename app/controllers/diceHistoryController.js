// Import user model vao controller
const userModel = require("../models/userModel");
const diceModel = require("../models/diceHistoryModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createDiceHistory = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let requestBody = request.body;

    console.log(requestBody);
    //B2 Validate dữ liệu

    //B3 Thao tác với cơ sỡ dữ liệu
    let diceInput = {
        _id: mongoose.Types.ObjectId(),
        user: requestBody.user,
        dice: requestBody.dice,
    }
    diceModel.create(diceInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Create dice Of user",
                data: data
            })

        }
    })
}


const getAllDiceHistory = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    diceModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(202).json({
                status: "Success: Get dices sucess",
                data: data
            })
        }
    })
}

const getDiceHistoryById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(diceHistoryId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "dice ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    diceModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: "Success: Get dice sucess",
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(diceHistoryId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "dice ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let diceUpdate = {
        user: bodyRequest.user,
        dice: bodyRequest.dice
    }
    diceModel.findByIdAndUpdate(diceHistoryId, diceUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: Update dice success",
                data: data
            })
        }
    })
}

const deleteDiceHistoryById = (request, response) => {//B1 Chuẩn bị dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(diceHistoryId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    diceModel.findByIdAndDelete(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(204).json({
                status: "Success: Delete users sucess",
            })
        }
    })
}

module.exports = {
    createDiceHistory: createDiceHistory,
    getAllDiceHistory: getAllDiceHistory,
    getDiceHistoryById: getDiceHistoryById,
    updateDiceHistoryById: updateDiceHistoryById,
    deleteDiceHistoryById: deleteDiceHistoryById

}