// Import voucher model vao controller
const voucherModel = require("../models/voucherModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createVoucher = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    console.log(bodyRequest);
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.code) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "code is required"
        })
    }
    if (!bodyRequest.discount) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "discount is required"
        })
    }
    //Thao tác với cơ sở dữ liệu
    let createvoucher = {
        _id: mongoose.Types.ObjectId(),
        code: bodyRequest.code,
        discount: bodyRequest.discount,
        note: bodyRequest.note
    }

    voucherModel.create(createvoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: voucher Created",
                data: data
            })
        }
    })

}

const getAllVoucher = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(202).json({
                status: "Success: Get vouchers sucess",
                data: data
            })
        }
    })
}

const getVoucherById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId;
    console.log(voucherId);
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: "Success: Get vouchers sucess",
                data: data
            })
        }
    })
}

const updateVoucherById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let voucherUpdate = {
        vcode: bodyRequest.code,
        discount: bodyRequest.discount,
        note: bodyRequest.note
    }
    voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: Update voucher success",
                data: data
            })
        }
    })
}

const deleteVoucherById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let voucherId = request.params.voucherId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(voucherId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "voucher ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(204).json({
                status: "Success: Delete vouchers sucess",
            })
        }
    })
}

module.exports = {
    createVoucher: createVoucher,
    getAllVoucher: getAllVoucher,
    getVoucherById: getVoucherById,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById

}