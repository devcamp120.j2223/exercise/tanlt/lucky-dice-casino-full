// Import user model vao controller
const userModel = require("../models/userModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createUser = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    console.log(bodyRequest);
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "username is required"
        })
    }
    if (!bodyRequest.firstname) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "firstname is required"
        })
    }
    if (!bodyRequest.lastname) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "lastname is required"
        })
    }

    //Thao tác với cơ sở dữ liệu
    let createuser = {
        _id: mongoose.Types.ObjectId(),
        username: bodyRequest.username,
        firstname: bodyRequest.firstname,
        lastname: bodyRequest.lastname
    }

    userModel.create(createuser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: user Created",
                data: data
            })
        }
    })

}

const getAllUser = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(202).json({
                status: "Success: Get users sucess",
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    console.log(userId);
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: "Success: Get users sucess",
                data: data
            })
        }
    })
}

const updateUserById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let userUpdate = {
        username: bodyRequest.username,
        firstname: bodyRequest.firstname,
        lastname: bodyRequest.lastname
    }
    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: Update user success",
                data: data
            })
        }
    })
}

const deleteUserById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(userId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(204).json({
                status: "Success: Delete users sucess",
            })
        }
    })
}

module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUserById: updateUserById,
    deleteUserById: deleteUserById

}