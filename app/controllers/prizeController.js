// Import prize model vao controller
const prizeModel = require("../models/prizeModel");

// Import mongooseJS
const mongoose = require("mongoose");

const createPrize = (request, response) => {
    //B1: Thu thập dữ liệu
    var bodyRequest = request.body;
    console.log(bodyRequest);
    //B2: Kiểm tra dữ liệu
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is required"
        })
    }

    //Thao tác với cơ sở dữ liệu
    let createprize = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        description: bodyRequest.description
    }

    prizeModel.create(createprize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: prize Created",
                data: data
            })
        }
    })

}

const getAllPrize = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    //B2 Validate dữ liệu
    //B3 Thao tác với cơ sỡ dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(202).json({
                status: "Success: Get prizes sucess",
                data: data
            })
        }
    })
}

const getPrizeById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let prizeId = request.params.prizeId;
    console.log(prizeId);
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: "Success: Get prizes sucess",
                data: data
            })
        }
    })
}

const updatePrizeById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let prizeId = request.params.prizeId;
    var bodyRequest = request.body;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    let prizeUpdate = {
        name: bodyRequest.name,
        description: bodyRequest.description
    }
    prizeModel.findByIdAndUpdate(prizeId, prizeUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(201).json({
                status: "Success: Update prize success",
                data: data
            })
        }
    })
}

const deletePrizeById = (request, response) => {
    //B1 Chuẩn bị dữ liệu
    let prizeId = request.params.prizeId;
    //B2 Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(prizeId))) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize ID is not valid"
        })
    }
    //B3 Thao tác với cơ sỡ dữ liệu
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        }
        else {
            return response.status(204).json({
                status: "Success: Delete prizes sucess",
            })
        }
    })
}

module.exports = {
    createPrize: createPrize,
    getAllPrize: getAllPrize,
    getPrizeById: getPrizeById,
    updatePrizeById: updatePrizeById,
    deletePrizeById: deletePrizeById

}