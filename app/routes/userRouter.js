
const express = require("express");

const router = express.Router();
const {randomNumberMiddleware } = require("../middlewares/randomNumberMiddleware")

const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/userController")

router.get("/random-number", randomNumberMiddleware);

router.get("/users", getAllUser);

router.post("/users", createUser);

router.get("/users/:userId", getUserById)

router.put("/users/:userId", updateUserById)

router.delete("/users/:userId", deleteUserById)

//Export dữ liệu thành 1 module
module.exports = router;