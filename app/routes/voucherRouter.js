
const express = require("express");

const router = express.Router();

const {createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById}= require("../controllers/voucherController")


router.post("/vouchers", createVoucher)

router.get("/vouchers", getAllVoucher)

router.get("/vouchers/:voucherId", getVoucherById)

router.put("/vouchers/:voucherId", updateVoucherById)

router.delete("/vouchers/:voucherId", deleteVoucherById)

//Export dữ liệu thành 1 module
module.exports = router;