
const express = require("express");

const router = express.Router();

const {createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById}= require("../controllers/prizeController")


router.post("/prizes", createPrize)

router.get("/prizes", getAllPrize)

router.get("/prizes/:prizeId", getPrizeById)

router.put("/prizes/:prizeId", updatePrizeById)

router.delete("/prizes/:prizeId", deletePrizeById)

//Export dữ liệu thành 1 module
module.exports = router;