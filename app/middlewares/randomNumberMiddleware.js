const randomNumberMiddleware = (request, response, next) => {
    return response.status(400).json({
        dice: Math.floor(Math.random() * (6 - 1 + 1) + 1)
    })
    next();
}
module.exports = { randomNumberMiddleware: randomNumberMiddleware }