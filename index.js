//Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

//Import mongooseJS
const mongoose = require("mongoose");

const path = require('path')

//Import router
const userRouter = require("./app/routes/userRouter.js");
const diceRouter = require("./app/routes/diceRouter.js");
const prizeRouter = require("./app/routes/prizeRouter.js");
const voucherRouter = require("./app/routes/voucherRouter.js");

//Khởi tạo app express
const app = express();

//Khai báo middleware đọc json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

const port = 8000;

mongoose.connect("mongodb://localhost:27017/Lucky_Dice_Casino", (error) => {
    if (error) {
        throw error
    };
    console.log('Connect MongooDB successfully!');
})


app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
})
app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

app.use(express.static(__dirname + '/views'));

//Khai báo API dạng get "/" sẽ chạy vào đây
//Callback funtion
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/Luckydice.html'));
})

app.use("/", userRouter);
app.use("/", diceRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng): " + port)
})